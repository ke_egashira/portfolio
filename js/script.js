$(function() {
	$('.links').hide();
	$('.links-wrapper').hover(function() {
	    $('.links').fadeToggle('fast');
	    return false;
    });
});

$(function() {
    $('.toggle-content-body').hide();
    $('.flex-item:nth-child(1)').click(function() {
        $('.flex-item:nth-child(1)').addClass('open');
	    $('.toggle-content-body:nth-child(1)').slideDown(400);
	    return false;
    });
});

$(function() {
	$('.flex-box').click(function() {
        $('.flex-item').removeClass('open');
        $('.toggle-content-body').slideUp('fast');
	});
});

$(function() {
    $('.form-body').hide();
    $('.sub-box p').click(function() {
        $('.sub-box').toggleClass('open');
	    $('.form-body').slideToggle(400);
	    return false;
    });
});
